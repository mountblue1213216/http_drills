const http = require('http');
const fs = require('fs').promises;
const { uuid } = require('uuidv4');

const PORT = 3000;

const server = http.createServer( async (request,response) => {
  try {

     if(request.url === '/'){
      return response.end("Server Healthy");
    } 

     if(request.url === '/html'){
    let file = await fs.readFile('./index.html');
    return response.end(file);
    }  

    if(request.url === '/json'){
      const jsonFile = await fs.readFile('./index.json','utf8');
      return response.end(jsonFile);
    }    

    if(request.url === '/uuid'){
      return response.end(JSON.stringify({"uuid" : uuid()}))
    }    

    if(request.url.startsWith('/status/')){
      let a = request.url.split('/')[2];
      request.statusCode = a;
      response.writeHead(request.statusCode,"Response with status code")
      return response.end(`Status Code = ${request.statusCode}`);
    } 

    if(request.url.startsWith('/delay/')){
      let delay = +request.url.split('/')[2] * 1000;
      setTimeout(()=>{
          return response.end(`Resonse after ${delay/1000} seconds`);
     },delay);
     
  }
  console.log("Request Received");
  } catch (error) {
    console.log(error);
  }
   
})

server.listen(PORT, () => {
    console.log(`Server started on PORT ${PORT}`);
})